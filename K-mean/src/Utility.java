import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;


import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class Utility extends Configured implements Tool{
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new Utility(), args);
		System.exit(res);
	}
	
	
	static public int[] convertStringToInts(String str) {
		String [] strs = str.split(" ");
		int[] results = new int[strs.length];
		
		for(int x=0; x<strs.length; ++x) {
			results[x]	= Integer.parseInt(strs[x]);
		}
		
		return results;
	}
	
	static public double[] convertStringToDoubles(String str) {
		String [] strs = str.split(" ");
		double[] results = new double[strs.length];
		
		for(int x=0; x<strs.length; ++x) {
			results[x]	= Double.parseDouble(strs[x]);
		}
		
		return results;
	}
	
	@Override
	public int run(String[] args) throws Exception {
		
		// Configuration object
		Configuration conf = this.getConf();
		// starting new job
	
		System.out.println(getFileSize(new Path(args[0]), conf));
		
		return 0;
	}
	
	static public long getFileSize(Path path, Job job) 
			throws IllegalArgumentException, IOException
	{
		long result = getFileSize(path, job.getConfiguration());
		return 	result;
	}
	
	static public long getFileSize(Path path, Configuration conf) 
			throws IllegalArgumentException, IOException
	{
		FileSystem fs = path.getFileSystem(conf);
		return 	fs.getFileStatus(path).getLen();
	}

}
