import java.io.IOException;

import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.Text;

// Usually Reducer will do, but if you really have to. Write a custom combiner won't hurt

// Check out hadoop's own Reducer collections before write your own!!
// FieldSelectionReducer, IntSumReducer, LongSumReducer, SecondarySort.Reduce, WordCount.IntSumReducer

public class CustomCombiner extends Reducer<Text, Text, Text, Text>{
	@Override
	protected void setup(Context context) 
		throws IOException, InterruptedException
	{
		// do your setup up code here
	}
	
	@Override
	protected void reduce(Text key, Iterable<Text> values, Context context)
		throws IOException, InterruptedException
	{
		// Do your reduce here
		// Output here
		// for(Text value: values) {
		//		context.write(key, value);
		// }
	}
	
	@Override
	protected void cleanup(Context context)
		throws IOException, InterruptedException
	{
		// do your clean up code here
	}
	
	// custom run method, if you don't know what does this do: 
	//
	//	****DONT TOUCH THEM, AT ALL****
	//
	//	In short it call setup(Context) at first,
	//	then invoke reduce(KEYIN, Interable<VALUEIN>) for each key/value pair, 
	//	then invoke cleanup(Context)
	//
	//	@Override
	//	public void run(Context context)
	//			throws IOException, InterruptedException
	//	{
	//		super.run(context);
	//	}
	
}
