import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;

// Check hadoop's impelementation of default Mappers before write your own!
// DelegatingMapper, FieldSelectionMapper, InverseMapper, MultithreadedMapper, SecondarySort.MapClass, TokenCounterMapper, WordCount.TokenizerMapper

public class KMeansMapper extends Mapper<LongWritable, Text, Text, Text>{
	List<Double[]> listing = new ArrayList<Double[]>();
	
	@Override
	protected void setup(Context context) 
		throws IOException, InterruptedException
	{
		URI[] cache_uri = DistributedCache.getCacheFiles(context.getConfiguration());
		Path cache_path = new Path(cache_uri[0]);
		FileSystem fs = FileSystem.get(cache_uri[0], context.getConfiguration());
		InputStream is = fs.open(cache_path);
		Scanner scanner = new Scanner(is);
		scanner.useDelimiter("\n");
		
		while(scanner.hasNext()) {
			String raw_data = scanner.next();
			if(raw_data.equals("")) {
				break;
			}
			String[] data = raw_data.split(" ");
			Double[] data_arr = new Double[2];
			data_arr[0] = Double.parseDouble(data[0]);
			data_arr[1] = Double.parseDouble(data[1]);
			listing.add(data_arr);
		}
		scanner.close();
		is.close();
	}
	
	@Override
	protected void map(LongWritable key, Text value, Context context)
		throws IOException, InterruptedException
	{
		Iterator<Double[]> iter = this.listing.iterator();
		double[] values = Utility.convertStringToDoubles(value.toString());
		
		double min = Double.MAX_VALUE;
		int min_center = 0;

		int count = 0;
		while(iter.hasNext()) {
			Double[] centers = iter.next();
			double sq_difference = 0;
			for(int x=0; x<centers.length; ++x) {
				sq_difference += (values[x] - centers[x]) * (values[x] - centers[x]);
			}
			double sqrt = Math.sqrt(sq_difference);
			
			if(sqrt < min) {
				min_center = count;
				min = sqrt;
			}
			++count;
		}
		System.out.println("Node: " + value.toString() + ": " +Integer.toString(min_center));
		context.write(new Text(Integer.toString(min_center)), value);
	}
	
	@Override
	protected void cleanup(Context context)
		throws IOException, InterruptedException
	{
		// do your clean up code here
	}
	
	// custom run method, if you don't know what does this do: 
	//
	//	****DONT TOUCH THEM, AT ALL****
	//
	//	In short it call setup(Mapper.Context) at first,
	//	then invoke map(KEYIN, VALUEIN) for each key/value pair, 
	//	then invoke cleanup(Mapper.Context)
	//
	//	@Override
	//	public void run(Mapper.Context context)
	//			throws IOException, InterruptedException
	//	{
	//		super.run(context);
	//	}
	
}
