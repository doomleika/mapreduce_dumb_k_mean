import java.net.URI;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class KMeansDriver extends Configured implements Tool{
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new KMeansDriver(), args);
		System.exit(res);
	}
	
	@Override
	public int run(String[] args) throws Exception {
		// Configuration object
		Configuration conf = this.getConf();
		
		// Set your program arguments here
		if(args.length != 3) {
			// Hardcoding input/output is usually a bad idea
			// Removing the following code if you wish to hardcode
			// Modifying the code if you wish to customize
			System.out.println("<Starting_Nodes> <Nodes> <Result>");
			System.exit(1);
		}
		String starting_nodes_file = args[0];
		String all_nodes_file = args[1];
		String result_file = args[2];
		
		Path starting_node_path = new Path(starting_nodes_file);
		URI starting_node_uri = starting_node_path.makeQualified(starting_node_path.getFileSystem(conf)).toUri();
		
		int passes = 0;

		// starting new job
		Job job = new Job(conf);
		job.setJarByClass(KMeansDriver.class);
		// Set your hadoop job name herez
		job.setJobName("KMeanDriver pass: " + Integer.toString(passes) + args[0] + " " + args[1] + " " + args[2]);
		
		// InputFormat dictates which type of input you want to use
		// Default uses TextInputFormat or you can use one of it's FileInputFormat cousin
		// CombineFileInputFormat, KeyValueTextInputFormat, NLineInputFormat, SequenceFileInputFormat, TextInputFormat
		// Or DBInputFormat for DB read
		// For more complex structure use DelegatingInputFormat
		// If you have to use your own, override FileInputFormat, or you have to do everything
		// (InputFormat, InputSplit, RecordReader) yourself. consult source code when you need guidance
		FileInputFormat.setInputPaths(job, new Path(all_nodes_file));
        job.setInputFormatClass(TextInputFormat.class);
        DistributedCache.addCacheFile(starting_node_uri, job.getConfiguration());
		
		// Set your custom mapper class here
		job.setMapperClass(KMeansMapper.class);
		// Output Key/Value class has to match your own mapper's class 
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		
		// Set your custom combiner class here(usually, reducer)
		// job.setCombinerClass(CustomCombiner.class);
		
		// Set reduce number here, 0 if this is map job only
		// Ususally use -Dmapred.reduce.tasks=num for it
		// job.setNumReduceTasks(1);
		
		// Set your own partitioner here
		//job.setPartitionerClass(CustomPartitioner.class);
		
		// Set your custom reducer class here
		job.setReducerClass(KMeansReducer.class);
		// Output Key/Value class this has to match reducer output key/value type
		// If this is a map only job, then it has to match mapper output key/value type
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		

        // Output
        FileOutputFormat.setOutputPath(job, new Path(result_file));
        job.setOutputFormatClass(TextOutputFormat.class);
		
        // use job.submit() if you wish to monitor job yourself.
		return job.waitForCompletion(true) ? 0 : 1;
	}

}
