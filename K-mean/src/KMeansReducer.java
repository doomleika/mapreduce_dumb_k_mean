import java.io.IOException;

import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.io.Text;


// Check out hadoop's own Reducer collections before write your own!!
// FieldSelectionReducer, IntSumReducer, LongSumReducer, SecondarySort.Reduce, WordCount.IntSumReducer

public class KMeansReducer extends Reducer<Text, Text, Text, Text>{
	@Override
	protected void setup(Context context) 
		throws IOException, InterruptedException
	{
		// do your setup up code here
	}
	
	@Override
	protected void reduce(Text key, Iterable<Text> values, Context context)
		throws IOException, InterruptedException
	{
		double[] total = new double[2];
		int value_count = 0;
		for(Text value: values) {
			double[] doubles = Utility.convertStringToDoubles(value.toString());
			
			total[0] += doubles[0]; 
			total[1] += doubles[1];

			++value_count;
		}
		total[0] = total[0] / value_count;
		total[1] = total[1] / value_count;
		
		String answer = Double.toString(total[0]) + " " + Double.toString(total[1]);
		context.write(new Text(""), new Text(answer));
	}
	
	@Override
	protected void cleanup(Context context)
		throws IOException, InterruptedException
	{
		// do your clean up code here
	}
	
	// custom run method, if you don't know what does this do: 
	//
	//	****DONT TOUCH THEM, AT ALL****
	//
	//	In short it call setup(Context) at first,
	//	then invoke reduce(KEYIN, Interable<VALUEIN>) for each key/value pair, 
	//	then invoke cleanup(Context)
	//
	//	@Override
	//	public void run(Context context)
	//			throws IOException, InterruptedException
	//	{
	//		super.run(context);
	//	}
	
}
